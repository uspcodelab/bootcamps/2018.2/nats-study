const Koa = require('koa'),
      app = new Koa(),
      router = require('koa-router')(),
      koaBody = require('koa-body');

const NATS = require('nats'),
      nc = NATS.connect({
        url: 'nats://nats:4222',
        preserveBuffers: true
      });

router.post('/', koaBody(),
  (ctx) => {
    // console.log(ctx.request.body);
    // => POST body
    ctx.body = JSON.stringify(ctx.request.body);
    console.log(ctx.body);
    
    let msg = ctx.body;
    let buf = Buffer.allocUnsafe(msg.length);
    buf.fill(msg);
    nc.publish("comm", buf);
  }
);

app.use(router.routes());

app.listen(3000);
console.log('Producer running!');
